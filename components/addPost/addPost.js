import { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useMutation } from "react-query";
import axios from "axios";
import { Modal } from "../modal";

const validationSchema = Yup.object().shape({
  title: Yup.string().min(2, "Too Short!").required("Required"),
  body: Yup.string().min(20, "Too Short!").required("Required"),
});

export function AddPost(props) {
  const [visible, setVisible] = useState(false);

  const addPostMutation = useMutation((values) => {
    return axios.post("/api/posts", JSON.stringify(values), {
      headers: {
        "Content-Type": "application/json",
      },
    });
  });

  const formik = useFormik({
    initialValues: {
      title: "",
      body: "",
    },
    validationSchema,
    onSubmit: async (values) => {
      await addPostMutation.mutateAsync(values);
      setVisible(false);
      props.onCreateSuccess();
    },
  });

  return (
    <>
      <div className="bg-gray-400">
        <button
          className="bg-red-400 py-3 px-4 rounded-lg font-semibold font-mono hover:bg-red-500 transition-all"
          onClick={() => setVisible(true)}
        >
          Create Post
        </button>
      </div>
      <Modal visible={visible}>
        <div className="w-96">
          <form onSubmit={formik.handleSubmit}>
            <div className="mb-4">
              <label
                className="block mb-2 text-center uppercase font-bold text-base"
                htmlFor="title"
              >
                Title
              </label>
              <input
                id="title"
                name="title"
                type="text"
                placeholder="Title"
                className="bg-red-400 p-2 rounded-md text-lg font-sans w-full placeholder:text-slate-600"
                onChange={formik.handleChange}
                value={formik.values.title}
              />
              <div className="mt-1 text-red-700 font-extrabold">
                {formik.errors.title}
              </div>
            </div>
            <div className="mb-4">
              <label
                className="block mb-2 text-center uppercase font-bold text-base"
                htmlFor="body"
              >
                Body
              </label>
              <textarea
                id="body"
                name="body"
                type="text"
                placeholder="Body"
                className="bg-red-400 p-2 rounded-md text-lg font-sans w-full placeholder:text-slate-600"
                onChange={formik.handleChange}
                value={formik.values.body}
              />
              <div className="text-red-700 font-extrabold">
                {formik.errors.body}
              </div>
            </div>
            <div className="text-center">
              <button
                type="submit"
                className="bg-orange-600 py-3 px-4 rounded-lg font-semibold font-mono hover:bg-orange-700 transition-all"
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </Modal>
    </>
  );
}
