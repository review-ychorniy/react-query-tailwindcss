import axios from "axios";
import { useMutation } from "react-query";
import { Modal } from "../modal";

export function RemovePost({ postId, onCancel, onSuccess }) {
  const removePostMutation = useMutation(() => {
    return axios.delete(`/api/posts/${postId}`);
  });

  const handleRemove = async () => {
    await removePostMutation.mutateAsync();
    onSuccess();
  };

  return (
    <Modal visible>
      <div className="p-12 ">
        <div className="text-3xl">Are you sure ?</div>
        <div className="mt-4 flex justify-around">
          <button
            className="bg-orange-400 py-3 px-4 rounded-lg font-semibold font-mono hover:bg-orange-500 transition-all"
            onClick={() => {
              onCancel();
            }}
          >
            No
          </button>
          <button
            className="ml-4 bg-red-400 py-3 px-4 rounded-lg font-semibold font-mono hover:bg-red-500 transition-all"
            onClick={handleRemove}
          >
            Yes
          </button>
        </div>
      </div>
    </Modal>
  );
}
