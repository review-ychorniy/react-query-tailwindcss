export function Modal({ visible, children }) {
  if (!visible) {
    return null;
  }

  return (
    <div className="fixed top-0 left-0 bottom-0 right-0">
      <div className="absolute top-2/4 left-2/4 -translate-x-2/4 -translate-y-2/4">
        <div className="min-w-60 min-he-32 bg-white p-4">{children}</div>
      </div>
    </div>
  );
}
