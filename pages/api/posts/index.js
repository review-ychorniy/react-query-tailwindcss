import { addPost, getPosts } from "../../../service/post.service";

export default function handler(req, res) {
  if (req.method === "POST") {
    addPost(req.body);
  }

  res.status(200).json({ success: true, posts: getPosts() });
}
