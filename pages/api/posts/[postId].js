import { getPost, updatePost, removePost } from "../../../service/post.service";

export default function handler(req, res) {
  if (req.method === "DELETE") {
    removePost(req.query.postId);
  } else if (req.method === "PUT") {
    updatePost(req.body);
  }

  res.status(200).json({ success: true, post: getPost(req.query.postId) });
}
