import { useState } from "react";
import { useQuery, useQueryClient } from "react-query";
import axios from "axios";
import { AddPost } from "../components/addPost";
import { UpdatePost } from "../components/updatePost";
import { RemovePost } from "../components/removePost";

export default function Home() {
  const [updatePostId, setUpdatePostId] = useState(false);
  const [removePostId, setRemovePostId] = useState(false);
  const queryClient = useQueryClient();

  const { isLoading: postsIsLoading, data: postsData = {}, error } = useQuery(
    "posts",
    () => {
      return axios.get("/api/posts");
    },
    {
      select: (resp) => resp.data,
    }
  );

  const { posts = [] } = postsData;

  return (
    <div>
      <div className="w-full h-16 bg-slate-500 flex items-center justify-center">
        <div className="font-mono text-white text-2xl lg:text-3xl">
          Posts App
        </div>
      </div>
      <AddPost onCreateSuccess={() => queryClient.invalidateQueries("posts")} />
      {updatePostId && (
        <UpdatePost
          postId={updatePostId}
          onCancel={() => setUpdatePostId(null)}
          onSuccess={() => {
            setUpdatePostId(null);
            queryClient.invalidateQueries("posts");
          }}
        />
      )}
      {removePostId && (
        <RemovePost
          postId={removePostId}
          onCancel={() => setRemovePostId(null)}
          onSuccess={() => {
            setRemovePostId(null);
            queryClient.invalidateQueries("posts");
          }}
        />
      )}
      <div className="p-4 bg-gray-400 min-h-[40rem]">
        {postsIsLoading && (
          <div className="animate-pulse flex items-center justify-center h-full">
            <div className="rounded-full bg-gray-200 h-40 w-40"></div>
          </div>
        )}
        <div className="flex flex-wrap">
          {posts.map((item) => {
            return (
              <div className="w-1/3 p-4 text-center" key={item.id}>
                <div className="bg-zinc-100 h-full p-2 rounded-md">
                  <div className="font-bold uppercase text-sm whitespace-nowrap text-ellipsis overflow-hidden">
                    {item.title}
                  </div>
                  <div className="flex justify-around mt-2 mb-2">
                    <button
                      className="bg-amber-400 py-3 px-4 rounded-lg font-semibold font-mono hover:bg-amber-500 transition-all"
                      onClick={() => {
                        setUpdatePostId(item.id);
                      }}
                    >
                      Update
                    </button>
                    <button
                      className="bg-red-400 py-3 px-4 rounded-lg font-semibold font-mono hover:bg-red-500 transition-all"
                      onClick={() => {
                        setRemovePostId(item.id);
                      }}
                    >
                      Remove
                    </button>
                  </div>
                  <div className="mt-2 text-justify break-words">
                    {item.body}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
