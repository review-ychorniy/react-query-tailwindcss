import posts from "../mocks/posts.json";

function generateUUID() {
  // Public Domain/MIT
  var d = new Date().getTime(); //Timestamp
  var d2 =
    (typeof performance !== "undefined" &&
      performance.now &&
      performance.now() * 1000) ||
    0; //Time in microseconds since page-load or 0 if unsupported
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = Math.random() * 16; //random number between 0 and 16
    if (d > 0) {
      //Use timestamp until depleted
      r = (d + r) % 16 | 0;
      d = Math.floor(d / 16);
    } else {
      //Use microseconds since page-load if supported
      r = (d2 + r) % 16 | 0;
      d2 = Math.floor(d2 / 16);
    }
    return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
  });
}

export function getPosts() {
  return posts;
}

export function getPost(postId) {
  return posts.find((item) => String(item.id) === String(postId));
}

export function addPost(post) {
  posts.unshift({
    id: generateUUID(),
    userId: null,
    ...post,
  });
}

export function updatePost(post) {
  posts = posts.map((item) => {
    if (String(item.id) === String(post.id)) {
      return {
        ...item,
        ...post,
      };
    }
    return item;
  });
}

export function removePost(postId) {
  posts = posts.filter((item) => String(item.id) !== String(postId));
}
